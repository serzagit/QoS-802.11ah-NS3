#Raw total duration
import sys

#slot_count = int(input("Slot count: "))
#num_slots = int(input("Num Slots in group: "))

rawFile = "./OptimalRawGroup/RawConfig-" + str(sys.argv[1]) + ".txt"

with open(rawFile, "r") as rf:
	lines = rf.readlines()
	
param = lines[2].split("\t")


rawD = (500 + (120*int(param[3])))*int(param[4])
nextINT = ((rawD*2) // 1024) + 1
beaconInterval = nextINT*1024
print(beaconInterval)
#print("Raw dur: " + str(rawD))
#print("Raw dur 1024us: " + str(nextINT*1024))
#print("Beacon dur 1024us: " + str(nextINT*1024*2))
