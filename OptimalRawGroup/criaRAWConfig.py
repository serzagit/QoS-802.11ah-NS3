print("\n      RAW configuration file creator        ")
print("##############################################")
op='0'
Ngrps=-1
Rctrl=-1
CSlotBound=-1
Sformat=-1
Scount=-1
Snum=-1
PageIdx=0
AIDstart=1
AIDend=10
invalid=True


while(op != '3'):
	print("\n\nOptions:")
	print("1. Help/Parameters Description")
	print("2. Create file and define parameters")
	print("3. Exit")
	print("Enter Op: ", end='')
	op = input()

	if op == '1':
		print("\n* RawControl - Whether RAW can be accessed by any stations within the RAW group or only the paged stations within the RAW group.\n" + 
		"* CrossSlotBoundary - Whether STAs are allowed to transmit after the assigned RAW slot boundary.\n" +
		"* SlotFormat - Format of RAW slot count.\n" +
		"* NRawSlotCount - Used to calculate number of RAW slot duration.\n" + 
		"* NRawSlotNum - Number of slots per RAW group.\n" +
		"* Page - Page index of the subset of AIDs.\n" +
		"* Aid_start - Station with the lowest AID allocated in the RAW.\n" +
		"* Aid_end - Station with the highest AID allocated in the RAW.")
		
	elif op == '2':
		AIDend = int(input("Final AID(total nodes): "))
		Ngrps = int(input("How many RAW groups ?: "))
		while Rctrl != 0 and Rctrl != 1:
			Rctrl = 0 #int(input("Raw Control -> 0(only paged) or 1(any): "))
			if Rctrl != 0 and Rctrl != 1:
				print("Invalid value !!!")
		while CSlotBound != 0 and CSlotBound != 1:
			CSlotBound = int(input("Cross Slot Boundary -> 0(not allowed) or 1(allowed): "))
			if CSlotBound != 0 and CSlotBound != 1:
				print("Invalid value !!!")
		while Sformat != 0 and Sformat != 1:
			Sformat = int(input("Slot Format -> 0(63 slots/255us) or 1(7 slots/2047us): "))
			if Sformat != 0 and Sformat != 1:
				print("Invalid value !!!")
		while invalid == True:
			Scount = int(input("Slot Duration count: "))
			Snum = AIDend // Ngrps #int(input("Number of slots per group: "))
			if(Sformat == 0 and Snum > 63):
				Snum = 63
			elif(Sformat == 1 and Snum > 7):
				Snum = 7
			if (Sformat == 0 and Scount > 255) or (Sformat == 1 and Scount > 2047):
				print("Invalid value for Slot Duration !!!")
				invalid = True
			elif(Sformat == 0 and Snum > 63) or (Sformat == 1 and Snum > 7):
				print("Invalid value for Slot Number !!!")
				invalid = True
			else:
				invalid = False
		PageIdx = 0 #int(input("Page index: "))
		AIDstart = 1 #int(input("Initial AID: "))
		op='3'
	elif op == '3':
		exit()

filename = "./OptimalRawGroup/RawConfig-" + str(AIDend) + ".txt"
RAWfile = open(filename, "a")
StaPerGroup = int(AIDend/Ngrps)

start = AIDstart
end = StaPerGroup

RAWfile.write(str(Ngrps)+"\n")
while end <= AIDend:
	RAWfile.write("1\n")
	RAWfile.write(str(Rctrl) + "\t" + str(CSlotBound) + "\t" + str(Sformat) + "\t" + str(Scount) + "\t" + str(Snum) + 
				"\t" + str(PageIdx) + "\t" + str(start) + "\t" + str(end))
	if end != AIDend:
		RAWfile.write("\n")
	start = start + StaPerGroup
	end = (start + StaPerGroup)-1

print ("FILE DONE !!!")






