

for nodes in range(260, 1100, 60):
	#print(i)
	for run in range(1, 31):
		filename = "../../Resultados_04032019/mixed/generalInfo_" + str(nodes) + "_" + str(run)+ ".txt"
		
		
		with open(filename, "r") as gf:
			lines = gf.readlines()
	
		param = lines[0].split(";")
		sensors = int(param[1])
		cameras = int(param[12])
		
		spg = int(sensors/9)
		#print(str(spg) + " - " + str(cameras))
		sspg = int(sensors%9)
		#print("#" + str(sspg))
		
		gname = "RawConfig-" + str(nodes) + "-" + str(run) + ".txt"
		Gfile = open(gname, "w+")
		

		Gfile.write("10\n")
		
		start=1
		end = spg
		for rg in range(1,11):
			if( rg < 10 ):
				Scount = 7
				if ( rg < 9 ):
					Snum = spg
				if ( rg == 9 ):
					Snum = spg + sspg
			elif (rg == 10):
				Scount = 19
				Snum = cameras
				
			if ( Snum > 63 ):
				Snum = 63
				
						
			if( rg < 10 and rg != 1):
				if ( rg < 9 ):
					start = start + spg
					end = (start + spg)-1
				if ( rg == 9 ):
					start = start + spg
					end = (start + (spg + sspg))-1
			elif (rg == 10):
				start = start + (spg + sspg)
				end = (start + cameras)-1
				
				
			Gfile.write("1\n")
			Gfile.write("0\t" + "0\t" + "0\t" + str(Scount) + "\t" + str(Snum) + 
				"\t" + "0\t" + str(start) + "\t" + str(end))

			if end != nodes:
				Gfile.write("\n")


print ("ALL FILES DONE !!!")
