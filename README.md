This repository hosts a modified version of the IEEE 802.11ah module for NS-3, implemented by Le Tian *et al.* in [1] and extended in [2]:

> [1] L. Tian, S. Deronne, S. Latré, and J. Famaey, **Implementation and Validation of an IEEE 802.11ah Module for ns-3**, Proc. Work. ns-3 - WNS3 ’16, no. January, pp. 49–56, 2016, doi: 10.1145/2915371.2915372.

> [2] Le Tian, Amina Sljivo, Serena Santi, Eli De Poorter, Jeroen Hoebeke, Jeroen Famaey. **Extension of the IEEE 802.11ah NS-3 Simulation Module.** Workshop on ns-3 (WNS3), 2018.


### IEEE 802.11ah ns-3 
According to the original authors, this alpha version comes pre-packaged with NS-3 version 3.23 but multiple modules are updated to the latest 3.25 version to incorporate congestion control fixes for TCP traffic.

This module includes support for:
* Restricted Access Window (RAW) with interface for dynamic configuration
* Traffic Indication Map (TIM) segmentation 
* Energy consumption model
* Adaptive Modulation and Coding Scheme (MCS)

### This modified version includes: ###
* Nodes distribution in RAW groups according to its type.
* A mechanism to dynamic adjust RAW partitions duration based on traffic.
* RPS file structure modification.
* Enable MAC QoS and EDCA queues.
* Free contention period after each RAW.
* Some minor modifications.

### Installation and usage instructions ###
* Clone the project from git
* Follow the instructions on https://www.nsnam.org/wiki/Installation to prepare all dependencies
* Change into QoS-802.11ah-NS3 directory.  
* Configure waf:
`CXXFLAGS="-std=c++11" ./waf configure --disable-examples --disable-tests`
* Build:
`./waf`
* Run the simulation
`./waf --run test`
    * Pass the arguments for desired setup e.g.:
    
    `./waf --run "test --seed=1 --simulationTime=60 --payloadSize=256"`

### Run example, with some parameters:

./waf --run "scratch/test/test" --seed=1 --runSeed=1 --simulationTime=90 --payloadSize=100 --payloadCamera=1500 --BeaconInterval=45056 --DataMode="MCS2_8" --datarate=7.8 --bandWidth=2 --rho="100" --RAWConfigFile="./OptimalRawGroup/RawConfig-80.txt" --TrafficType="groups" --DynamicSlots="on" --RandPktSize="on" --pagePeriod=1 --pageSliceLength=31 --pageSliceCount=0

    Some new parameters added in this modified version:
    *seed - define general random seed
    *runSeed - define a random seed for running (see random variables section in NS-3 manual)
    *payloadSize - payload for sensors
    *payloadCamera - payload for camera
    *TrafficType [mix | groups]- define how nodes will be organized in RAWs, random assigned for RAW or in dedicated groups
    *DynamicSlots [on | off] - enable or disable dynamic adjust of RAW partitions duration according to traffic
    *RandPktSize [on | off] - use fixed size for packets or random size

#Using GDB with NS-3 if you want to debug
CXXFLAGS="-std=c++11 -g" ./waf configure -d debug --disable-examples --disable-tests
./waf --run "scratch/test/test" --command-template="gdb %s"

run --seed=1 --runSeed=1 --simulationTime=90 --payloadSize=100 --payloadCamera=1500 --BeaconInterval=45056 --DataMode="MCS2_8" --datarate=7.8 --bandWidth=2 --rho="100" --RAWConfigFile="./OptimalRawGroup/RawConfig-80.txt" --TrafficType="groups" --DynamicSlots="on" --RandPktSize="on" --pagePeriod=1 --pageSliceLength=31 --pageSliceCount=0
  
### RAW related parameters (modified from original 802.11ah ns-3 module): ###

| RAW Parameter | Value | Meaning |
| ------ | ------ | ------ |
| RAW control | 0 | RAW can be accessed by any nodes within the RAW group. |
| Cross slot boundary | 0 | If 0, nodes cannot cross slot boundary. If 1, node can cross. |
| Slot format | 0 | If 0, defines 6 bits for slot number inside a RAW (max.: 63 slots). If 1, defines 8 bits (max.: 255 slots). |
| Slot duration count | SDC | Used to calculate slot duration inside RAW. |
| Page | 0 | Index of AIDs subset. |
| AID Start |〖AID〗_S | Node with the smallest AID in RAW group. |
| AID End | 〖AID〗_E | Node with the biggest AID in RAW group. |

This file consists of:

First line = number of RAW groups

    For each RAW group:
        First line = number of RPS (Raw parameter set)
        Second line = RAW group configuration (according to RAW related parameter described above)

Example for 80 nodes in 10 RAW groups:
```
10
1
0	0	0	20	7	0	1	7
1
0	0	0	20	7	0	8	14
1
0	0	0	20	7	0	15	21
1
0	0	0	20	7	0	22	28
1
0	0	0	20	7	0	29	35
1
0	0	0	20	7	0	36	42
1
0	0	0	20	7	0	43	49
1
0	0	0	20	7	0	50	56
1
0	0	0	20	8	0	57	64
1
0	0	0	20	16	0	65	80
```

*Notes:                
RAW group duration = 500 us + SDC * 120 us, SDC is y = 11(8) bits length when Slot format is set to 1, and SDC is (14 - y) bits length, if Slot format is set to 0.  

- Useful scripts included (only for this modified version):

    | Script name | Function |
    | ------ | ------ |
    | criaRAWconfig.py | Helps with automatic creation of RAW config files. |
    | exec-tests.sh | Helps with automatic execution of multiple ns-3 simulation instances. |
    | rawduration.py | Calculates RAW group duration based on slot count and slot duration from RAW config files. |
    
### Wi-Fi mode parameters ###
* DataMode:           Data mode.  
* datarate:           Data rate of data mode.  
* bandWidth:          BandWidth of data mode.  

Note: Relation between the above 3 parameters and MCS is described in file "MCStoWifiMode".       

### TIM and page slice parameters ###
* pagePeriod:       Number of Beacon Intervals between DTIM beacons that carry Page Slice element for the associated page
* pageIndex:        Index of the page (0-3) whose slices are served during beacon intervals within a page period, default value is 0
* pageSliceLength:  Number of blocks in each TIM for the associated page except for the last TIM (1-31)
* pageSliceCount:   Number of TIMs in a single page period (0-31), value 0 has special meaning
* blockOffset:      The 1st page slice starts with the block with blockOffset number, default value is 0
* timOffset:        Offset in number of Beacon Intervals from the DTIM that carries the first page slice of the page, default value is 0

    Example run: ./waf --run "test --pagePeriod=4 --pageSliceLength=8 --pageSliceCount=4"
    
    * pagePeriod=4: every 4th beacon is DTIM beacon that carries Page Slice element for pageIndex=0
    * pageSliceLength=8: each page slice cosists of 8 blocks, meaning that each page slice (PS) accomodates up to 512 stations (8 blocks * 8 subblocks * 8 stations)
      * (slice0: 1-512, slice1: 513-1024, slice2: 1025-1536, slice3: 1537-2048)
      * The last page slice can have different length.
   * pageSliceCount=4: 4 TIMs are scheduled in one page period.
    
    Note: RAW configuration must be in line with TIM and page configuration. If a RAW group is reserved for a station in beacon interval that does not correspond to its TIM, station will be asleep during that RAW.
    
    To configure a single page slice (whole page encoded in a single page slice), it is neccessary to set pageSliceCount to 0 and pageSliceLength to 1.
