/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TimestampTag.h
 * Author: serza
 *
 * Created on 16 de Junho de 2018, 21:23
 */

#ifndef TIMESTAMPTAG_H
#define TIMESTAMPTAG_H

#include "ns3/application.h"
#include "ns3/event-id.h"
#include "ns3/ptr.h"
#include "ns3/address.h"
#include "ns3/traced-callback.h"
#include "ns3/tag.h"

namespace ns3{

class TimestampTag : public Tag {
   public:
    static TypeId GetTypeId (void);
    virtual TypeId GetInstanceTypeId (void) const;
   
    virtual uint32_t GetSerializedSize (void) const;
    virtual void Serialize (TagBuffer i) const;
    virtual void Deserialize (TagBuffer i);
  
    // these are our accessors to our tag structure
    void SetTimestamp (Time time);
    Time GetTimestamp (void) const;
 
    void Print (std::ostream &os) const;
  
   private:
    Time m_timestamp;
    // end class TimestampTag
};

} //namespace ns3

#endif /* TIMESTAMPTAG_H */

