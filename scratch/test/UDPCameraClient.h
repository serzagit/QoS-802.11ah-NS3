/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UDPCameraClient.h
 * Author: serza
 *
 * Created on 26 de Junho de 2018, 14:56
 */

#ifndef UDPVIDEOCLIENT_H
#define UDPVIDEOCLIENT_H

#include "ns3/address.h"
#include "ns3/application.h"
#include "ns3/event-id.h"
#include "ns3/ptr.h"
#include "ns3/data-rate.h"
#include "ns3/traced-callback.h"

namespace ns3{

class Address;
class RandomVariableStream;
class Socket;

class UDPCameraClient: public Application {
public:
	UDPCameraClient();
	virtual ~UDPCameraClient();

	static ns3::TypeId GetTypeId (void);

	typedef void (* DataSentCallback)
		                      (uint16_t nrOfBytes);

	typedef void (* StreamStateChangedCallback)
			                      (bool newStateIsStreaming);
        
        void SetMaxBytes (uint32_t maxBytes);

        /**
         * \brief Return a pointer to associated socket.
         * \return pointer to associated socket
         */
        Ptr<Socket> GetSocket (void) const;

       /**
        * \brief Assign a fixed random variable stream number to the random variables
        * used by this model.
        *
        * \param stream first stream index to use
        * \return the number of stream indices assigned by this model
        */
        int64_t AssignStreams (int64_t stream);

protected:
	virtual void StartApplication(void);
	virtual void StopApplication(void);
	//virtual void OnDataReceived();
        virtual void DoDispose (void);

private:

	void CheckMovement();
        
        //helpers
        /**
         * \brief Cancel all pending events.
         */
        void CancelEvents ();

        // Event handlers
        /**
         * \brief Start an On period
         */
        void StartSending ();
        /**
         * \brief Start an Off period
         */
        void StopSending ();
        /**
         * \brief Send a packet
         */
        void SendPacket ();
        
        /**
        * \brief Schedule the next packet transmission
        */
       void ScheduleNextTx ();
       /**
        * \brief Schedule the next On period start
        */
       void ScheduleStartEvent ();
       /**
        * \brief Schedule the next Off period start
        */
       void ScheduleStopEvent ();
       /**
        * \brief Handle a Connection Succeed event
        * \param socket the connected socket
        */
       void ConnectionSucceeded (Ptr<Socket> socket);
       /**
        * \brief Handle a Connection Failed event
        * \param socket the not connected socket
        */
       void ConnectionFailed (Ptr<Socket> socket);

        
        Ptr<Socket>     m_socket;       //!< Associated socket
        Address         m_peer;         //!< Peer address
        bool            m_connected;    //!< True if connected
        Ptr<RandomVariableStream>  m_onTime;       //!< rng for On Time
        Ptr<RandomVariableStream>  m_offTime;      //!< rng for Off Time
        DataRate        m_cbrRate;      //!< Rate that data is generated
        DataRate        m_cbrRateFailSafe;      //!< Rate that data is generated (check copy)
        uint32_t        m_pktSize;      //!< Size of packets
        uint32_t        m_residualBits; //!< Number of generated, but not sent, bits
        Time            m_lastStartTime; //!< Time last packet sent
        uint32_t        m_maxBytes;     //!< Limit total number of bytes sent
        uint32_t        m_totBytes;     //!< Total bytes sent so far
        EventId         m_startStopEvent;     //!< Event id for next start or stop event
        EventId         m_sendEvent;    //!< Event id of pending "send packet" event
        TypeId          m_tid;          //!< Type of the socket used
        
        Time m_interval;
	double m_motionProbability;
	Time m_motionDuration;
	int m_datarate;
        EventId m_schedule;
        
        Time motionStartedOn;
	bool motionActive = false;
        
        uint32_t m_sent; //!< Counter for sent packets
        uint8_t m_ac; //!< access category of a packet (add by Serza)

	Ptr<ns3::UniformRandomVariable> m_rv;

	TracedCallback<uint16_t> dataSent;
         
        /// Traced Callback: transmitted packets.
        TracedCallback<Ptr<const Packet> > m_txTrace;

	TracedCallback<bool> streamStateChanged;
};

}//namespace ns3
#endif /* UDPVIDEOCLIENT_H */

