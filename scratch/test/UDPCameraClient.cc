/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "ns3/log.h"
#include "ns3/address.h"
#include "ns3/inet-socket-address.h"
#include "ns3/inet6-socket-address.h"
#include "ns3/packet-socket-address.h"
#include "ns3/node.h"
#include "ns3/nstime.h"
#include "ns3/data-rate.h"
#include "ns3/double.h"
#include "ns3/random-variable-stream.h"
#include "ns3/socket.h"
#include "ns3/simulator.h"
#include "ns3/socket-factory.h"
#include "ns3/packet.h"
#include "ns3/uinteger.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/udp-socket-factory.h"
#include "ns3/string.h"
#include "ns3/pointer.h"
#include "ns3/qos-tag.h"
#include "ns3/seq-ts-header.h"
#include "ns3/delay-jitter-estimation.h"
#include "UDPCameraClient.h"
#include "src/network/helper/delay-jitter-estimation.h"



using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("UDPCameraClient");

NS_OBJECT_ENSURE_REGISTERED (UDPCameraClient);

UDPCameraClient::UDPCameraClient()
   :m_socket (0),
    m_sent(0),
    m_connected (false),
    m_residualBits (0),
    m_lastStartTime (Seconds (0)),
    m_totBytes (0)
{
    NS_LOG_FUNCTION (this);
}

UDPCameraClient::~UDPCameraClient()
{
   NS_LOG_FUNCTION (this);
}
void 
UDPCameraClient::SetMaxBytes (uint32_t maxBytes)
{
  NS_LOG_FUNCTION (this << maxBytes);
  m_maxBytes = maxBytes;
}

Ptr<Socket>
UDPCameraClient::GetSocket (void) const
{
  NS_LOG_FUNCTION (this);
  return m_socket;
}

int64_t 
UDPCameraClient::AssignStreams (int64_t stream)
{
  NS_LOG_FUNCTION (this << stream);
  m_onTime->SetStream (stream);
  m_offTime->SetStream (stream + 1);
  return 2;
}

void
UDPCameraClient::DoDispose (void)
{
  NS_LOG_FUNCTION (this);

  m_socket = 0;
  // chain up
  Application::DoDispose ();
}

ns3::TypeId UDPCameraClient::GetTypeId(void) {
	static ns3::TypeId tid = ns3::TypeId("UDPCameraClient")
			.SetParent<Application>()
                        .SetGroupName("Applications")
			.AddConstructor<UDPCameraClient>()
			.AddAttribute ("Interval",
			                   "The time to check for motion",
			                   TimeValue (Seconds (1.0)),
			                   MakeTimeAccessor (&UDPCameraClient::m_interval),
			                   MakeTimeChecker ())
                        .AddAttribute("MotionPercentage",
                                           "The probability motion occurred when checking",
                                           DoubleValue(0.01),
                                           MakeDoubleAccessor(&UDPCameraClient::m_motionProbability),
                                           MakeDoubleChecker<double>(0.0,1.0))
                        .AddAttribute("MotionDuration",
					   "The time to stream data when motion is detected",
					   TimeValue (Seconds (60.0)),
					   MakeTimeAccessor (&UDPCameraClient::m_motionDuration),
                                           MakeTimeChecker ())     
                    .AddAttribute ("DataRate", "The data rate in on state.",
                           DataRateValue (DataRate ("500kb/s")),
                           MakeDataRateAccessor (&UDPCameraClient::m_cbrRate),
                           MakeDataRateChecker ())
                    .AddAttribute ("PacketSize", "The size of packets sent in on state",
                           UintegerValue (512),
                           MakeUintegerAccessor (&UDPCameraClient::m_pktSize),
                           MakeUintegerChecker<uint32_t> (1))
                    .AddAttribute ("Remote", "The address of the destination",
                           AddressValue (),
                           MakeAddressAccessor (&UDPCameraClient::m_peer),
                           MakeAddressChecker ())
                    .AddAttribute ("OnTime", "A RandomVariableStream used to pick the duration of the 'On' state.",
                           StringValue ("ns3::ConstantRandomVariable[Constant=1.0]"),
                           MakePointerAccessor (&UDPCameraClient::m_onTime),
                           MakePointerChecker <RandomVariableStream>())
                    .AddAttribute ("OffTime", "A RandomVariableStream used to pick the duration of the 'Off' state.",
                           StringValue ("ns3::ConstantRandomVariable[Constant=1.0]"),
                           MakePointerAccessor (&UDPCameraClient::m_offTime),
                           MakePointerChecker <RandomVariableStream>())
                    .AddAttribute ("MaxBytes", 
                           "The total number of bytes to send. Once these bytes are sent, "
                           "no packet is sent again, even in on state. The value zero means "
                           "that there is no limit.",
                           UintegerValue (0),
                           MakeUintegerAccessor (&UDPCameraClient::m_maxBytes),
                           MakeUintegerChecker<uint32_t> ())
                    .AddAttribute ("Protocol", "The type of protocol to use.",
                           TypeIdValue (UdpSocketFactory::GetTypeId ()),
                           MakeTypeIdAccessor (&UDPCameraClient::m_tid),
                           MakeTypeIdChecker ())
                    .AddAttribute ("AccessCategory",
                            "The access category of a packet:" 
                            "AC_BK (Background 1[lp]) | AC_BK (Background 2[hp]), "
                            "AC_BE(Best Effort 0[lp]) | AC_BE(Excellent Effort 3[hp]),"
                            "AC_VI(Controled Load 4[lp]) | AC_VI (Video < 100ms latency and jitter 5[hp]),"
                            "AC_VO(Voice <10ms latency and jitter 6[lp]) | AC_VO (Network Control 7[hp]))",
                             UintegerValue(0),
                             MakeUintegerAccessor (&UDPCameraClient::m_ac),
                             MakeUintegerChecker<uint8_t> ())
                    .AddTraceSource("DataSent",
                           "Occurs when data is sent to the server",
                           MakeTraceSourceAccessor(&UDPCameraClient::dataSent),
                           "TCPIPCameraClient::DataSentCallback")
                    .AddTraceSource ("Tx", "A new packet is created and is sent",
                           MakeTraceSourceAccessor (&UDPCameraClient::m_txTrace),
                           "ns3::Packet::TracedCallback")
                    .AddTraceSource("StreamStateChanged",
                           "Occurs when either started or stopped streaming",
			   MakeTraceSourceAccessor(&UDPCameraClient::streamStateChanged),
			   "TCPIPCameraClient::StreamStateChangedCallback")

	;
	return tid;
}

// Private helpers
void UDPCameraClient::ScheduleNextTx ()
{
  NS_LOG_FUNCTION (this);

  if ((Simulator::Now() - motionStartedOn) < m_motionDuration)
    {
      uint32_t bits = m_pktSize * 8 - m_residualBits;
      NS_LOG_LOGIC ("bits = " << bits);
      //uint64_t br = m_cbrRate.GetBitRate();
      //std::cout << "BIT RATE =============" << long(br) << std::endl;
      
      Time nextTime (Seconds (bits / static_cast<double>(m_cbrRate.GetBitRate ()))); // Time till next packet
      NS_LOG_LOGIC ("nextTime = " << nextTime);
      
      m_sendEvent = Simulator::Schedule (nextTime, &UDPCameraClient::SendPacket, this);
    }
  else // motion duration has expired
    { 
      //std::cout << "No motion for " << m_motionDuration << ", stopping stream" << std::endl;
      motionActive = false;
      streamStateChanged(false);
      StopSending ();
    }
}

void UDPCameraClient::SendPacket ()
{
  NS_LOG_FUNCTION (this);

  NS_ASSERT (m_sendEvent.IsExpired ());
  
  SeqTsHeader seqTs;
  seqTs.SetSeq (m_sent);
  seqTs.SetTs(Simulator::Now());
  Ptr<Packet> packet = Create<Packet> (m_pktSize-(8+4)); // 8+4 : the size of the seqTs header (32 bits for seq Number and 64 bits for timeStamp)
  packet->AddHeader(seqTs);
  
  //add QosTag for packet (by Serza)
  QosTag qosTag;
  qosTag.SetTid (m_ac);
  packet->AddPacketTag (qosTag);
  
  //Delay jitter estimation (by Serza)
  DelayJitterEstimation::PrepareTx(packet);
  
  
  int statSend = m_socket->Send (packet);
  m_totBytes += m_pktSize;
  if (InetSocketAddress::IsMatchingType (m_peer))
    {
      NS_LOG_INFO ("At time " << Simulator::Now ().GetSeconds ()
                   << "s on-off application sent "
                   <<  packet->GetSize () << " bytes to "
                   << InetSocketAddress::ConvertFrom(m_peer).GetIpv4 ()
                   << " port " << InetSocketAddress::ConvertFrom (m_peer).GetPort ()
                   << " total Tx " << m_totBytes << " bytes");
    }
  else if (Inet6SocketAddress::IsMatchingType (m_peer))
    {
      NS_LOG_INFO ("At time " << Simulator::Now ().GetSeconds ()
                   << "s on-off application sent "
                   <<  packet->GetSize () << " bytes to "
                   << Inet6SocketAddress::ConvertFrom(m_peer).GetIpv6 ()
                   << " port " << Inet6SocketAddress::ConvertFrom (m_peer).GetPort ()
                   << " total Tx " << m_totBytes << " bytes");
    }
  if (statSend >= 0)
    {
      ++m_sent;
      m_txTrace (packet);
      NS_LOG_INFO ("TraceDelay TX " << packet->GetSize() << " bytes to "
                                    << InetSocketAddress::ConvertFrom(m_peer).GetIpv4 () << " Uid: "
                                    << packet->GetUid () << " Time: "
                                    << (Simulator::Now ()).GetSeconds ());

    }
  else
    {
      std::cout <<  "ERRRRRRRRROOOOOOORRRRRRRRR" << std::endl;
      NS_LOG_INFO ("Error while sending " << packet->GetSize() << " bytes to "
                                          << InetSocketAddress::ConvertFrom(m_peer).GetIpv4 ());
    }
  
  m_lastStartTime = Simulator::Now ();
  m_residualBits = 0;
  ScheduleNextTx ();
}


void UDPCameraClient::ScheduleStartEvent ()
{  // Schedules the event to start sending data (switch to the "On" state)
  NS_LOG_FUNCTION (this);
  //Time offInterval = Seconds (m_offTime->GetValue ());
  //NS_LOG_LOGIC ("start at " << offInterval);
  m_schedule = Simulator::Schedule(m_interval, &UDPCameraClient::CheckMovement, this);
}


// NAO UTILIZADO AINDA
void UDPCameraClient::ScheduleStopEvent ()
{  // Schedules the event to stop sending data (switch to "Off" state)
  NS_LOG_FUNCTION (this);

  Time onInterval = Seconds (m_onTime->GetValue ());
  NS_LOG_LOGIC ("stop at " << onInterval);
  m_schedule = Simulator::Schedule (onInterval, &UDPCameraClient::StopSending, this);
}

void UDPCameraClient::StartApplication(void) {
        NS_LOG_FUNCTION (this);

  // Create the socket if not already
  if (!m_socket)
    {
      m_socket = Socket::CreateSocket (GetNode (), m_tid);
      if (Inet6SocketAddress::IsMatchingType (m_peer))
        {
          m_socket->Bind6 ();
        }
      else if (InetSocketAddress::IsMatchingType (m_peer) ||
               PacketSocketAddress::IsMatchingType (m_peer))
        {
          m_socket->Bind ();
        }
      m_socket->Connect (m_peer);
      m_socket->SetAllowBroadcast (true);
      m_socket->ShutdownRecv ();

      m_socket->SetConnectCallback (
        MakeCallback (&UDPCameraClient::ConnectionSucceeded, this),
        MakeCallback (&UDPCameraClient::ConnectionFailed, this));
    }
  m_cbrRateFailSafe = m_cbrRate;

  // Insure no pending event
  CancelEvents ();
  // If we are not yet connected, there is nothing to do here
  // The ConnectionComplete upcall will start timers at that time
  //if (!m_connected) return;
  m_rv = CreateObject<UniformRandomVariable> ();
  m_sent=0;
  
  ScheduleStartEvent ();
  
}

void UDPCameraClient::StopApplication(void) {
    
    CancelEvents ();
    
    if(m_socket != 0)
    {
      m_socket->Close ();
    }
    else
    {
      NS_LOG_WARN ("Found null socket to close in StopApplication");
    }
}

// Event handlers
void UDPCameraClient::StartSending ()
{
  NS_LOG_FUNCTION (this);
  m_lastStartTime = Simulator::Now ();
  ScheduleNextTx ();  // Schedule the send packet event

}

void UDPCameraClient::StopSending ()
{
  NS_LOG_FUNCTION (this);
  CancelEvents ();
  //if(Simulator::Now() < NanoSeconds(30000000000)){
    ScheduleStartEvent ();
  //}
}

void UDPCameraClient::CheckMovement() {
    
    NS_LOG_FUNCTION (this);

    if(m_rv->GetValue(0,1) < m_motionProbability) {
        // start or reset motion started
        motionStartedOn = Simulator::Now();
        if(!motionActive) {
            //std::cout << "Motion detected, starting camera stream" << std::endl;
            motionActive = true;
            streamStateChanged(true);
            StartSending(); //Stream();
        }
        else {
            //std::cout << "Motion detected, resetting motion timer" << std::endl;
        }
    }

    ScheduleStartEvent();
}

void UDPCameraClient::CancelEvents ()
{
  NS_LOG_FUNCTION (this);

  if (m_sendEvent.IsRunning () && m_cbrRateFailSafe == m_cbrRate )
    { // Cancel the pending send packet event
      // Calculate residual bits since last packet sent
      Time delta (Simulator::Now () - m_lastStartTime);
      int64x64_t bits = delta.To (Time::S) * m_cbrRate.GetBitRate ();
      m_residualBits += bits.GetHigh ();
    }
  m_cbrRateFailSafe = m_cbrRate;
  Simulator::Cancel(m_schedule);
  Simulator::Cancel (m_sendEvent);
  Simulator::Cancel (m_startStopEvent);
  return;
}

void UDPCameraClient::ConnectionSucceeded (Ptr<Socket> socket)
{
  NS_LOG_FUNCTION (this << socket);
  m_connected = true;
}

void UDPCameraClient::ConnectionFailed (Ptr<Socket> socket)
{
  NS_LOG_FUNCTION (this << socket);
}

/*
void UDPCameraClient::Stream() {
	// stop transmitting if duration of motion has expired
        //std::cout << "Diff =  " << (Simulator::Now() - motionStartedOn) << "Dur = " << m_motionDuration << std::endl;
	if((Simulator::Now() - motionStartedOn) > m_motionDuration) {
		std::cout << "No motion for " << m_motionDuration << ", stopping stream" << std::endl;
		motionActive = false;
		streamStateChanged(false);
		return;
	}

	int msPerSend = 100;
	// data to send every 100 ms is (DataRate * 1024 / 8) / 10 bytes

	int bytesToSend = (m_datarate * 1024 / 8) / (1000/msPerSend);

	char* buffer = new char[bytesToSend];
	for(int i = 0; i < bytesToSend; i++) {
		buffer[i] = i % 256;
	}
        i=i+1;
	//std::cout << "Writing " << bytesToSend << " to buffer " << "[" << i << "]"<< std::endl;
	Write(buffer, bytesToSend);
	dataSent(bytesToSend);

	delete buffer;

	ns3::Simulator::Schedule(MilliSeconds(msPerSend), &TCPIPCameraClient::Stream, this);
}
 * 
void UDPCameraClient::OnDataReceived() {

	char* buf = new char[1024];
	while(int nrOfBytesRead = Read(buf, 1024)) {
	}
	delete buf;

	// not expecting any data from the server
}
*/

