#!/bin/bash

if [ "$#" -lt 4 ]; then
	echo -e "Usage: $0 [num of nodes] [num of repetitions] [mix | groups] [lmcad | cluster] \n"
	exit -1
fi

if [ "$4" == "cluster" ]; then
	PYTHON36="/usr/bin/python3.5"
elif [ "$4" == "lmcad" ]; then
	PYTHON36="/usr/local/bin/python3.6"
else
	echo -e "No valid option for local\n"
	exit -1
fi

if [ "$3" == "mix" ]; then
	TRAFFIC_TYPE="mix"
elif [ "$3" == "groups" ]; then
	TRAFFIC_TYPE="groups"
else 
	echo -e "No valid option for grouping strategy\n"
	exit -1
fi


DATA_MODE=("MCS2_8")
DATA_RATE=(7.8) # in Megabits
BANDWIDTH=(2) # in Mhz
NUM_RAW_STA=($1) #(20 40 80 160 320 640 1280)
MAX_DIST=("100") # in meters: rho


# random in shell script $((($RANDOM % 63 ) + 1))

PAY_BK=(100) # payload in bytes
PAY_BE=(100)
PAY_VI=(1500)
PAY_VO=(100)
SIMULATION=(180) # simulation time in seconds

BEACON_INT=$($PYTHON36 rawduration.py $1)

#echo ${BEACON_INT}

for run in $(seq 1 $2);
do
	NS3_OUTPUT="./Saidas/SAIDA_$1_$run.txt"
	RAW_FILE="./OptimalRawGroup/RawConfig-$1-$run.txt"

	echo "Exec [$run] - Simulating for $1 nodes"

	./waf --run "scratch/test/test --seed=1 --runSeed=$run --simulationTime=${SIMULATION[0]} --payloadSize=${PAY_BE[0]} --payloadCamera=${PAY_VI[0]} --BeaconInterval=${BEACON_INT} --DataMode=${DATA_MODE[0]} --datarate=${DATA_RATE[0]} --bandWidth=${BANDWIDTH[0]} --rho=${MAX_DIST[0]} --RAWConfigFile=${RAW_FILE}  --TrafficType=${TRAFFIC_TYPE} --pagePeriod=1 --pageSliceLength=31 --pageSliceCount=0" >> $NS3_OUTPUT&
	
	sleep 2
	#echo -e "\n"

done
