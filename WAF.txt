./waf --run "scratch/80211ah-test --NRawSta=4 --NGroup=4 --SlotFormat=0 --NRawSlotCount=162 --NRawSlotNum=5 --DataMode="OfdmRate650KbpsBW2MHz" --datarate=0.65 --bandWidth=2 --rho="50" --simulationTime=60 --payloadSize=1500 --BeaconInterval=100000 --UdpInterval=0.1 --Nsta=4 --file="./TestMac/mac-sta.txt"  --pcapfile="./TestMac/mac-sta" --seed=1"


./waf --run "scratch/80211ah-test --NRawSta=30 --NGroup=30 --SlotFormat=0 --NRawSlotCount=162 --NRawSlotNum=5 --DataMode="OfdmRate70_2MbpsBW16MHz" --datarate=70.2 --bandWidth=16 --rho="50" --simulationTime=60 --payloadSize=1500 --BeaconInterval=100000 --UdpInterval=0.1 --Nsta=30 --file="./TestMac/mac-sta.txt"  --pcapfile="./TestMac/mac-sta" --seed=1"

./waf --run "scratch/80211ah-test --NRawSta=30 --NGroup=30 --SlotFormat=0 --NRawSlotCount=162 --NRawSlotNum=5 --DataMode="OfdmRate70_2MbpsBW16MHz" --datarate=70.2 --bandWidth=16 --rho="50" --simulationTime=60 --payloadBK=100 --payloadBE=256 --payloadVI=1500 --payloadVO=100 --BeaconInterval=100000 --UdpInterval=0.1 --Nsta=30 --file="./Results/results.txt"  --pcapfile="./Results/mac-sta" --OutputData="False" --seed=1"


saida.txt > 2>&1

CXXFLAGS="-std=c++0x" ./waf configure --disable-examples --disable-tests


./waf --run "scratch/test/test --seed=1 --simulationTime=60 --payloadSize=100 --payloadCamera=1500 --DataMode="MCS2_0" --datarate=0.65 --bandWidth=2 --rho="100" --TrafficType="CameraAndSensor" --pagePeriod=4 --pageSliceLength=4 --pageSliceCount=2"

./waf --run "scratch/test/test --seed=1 --simulationTime=60 --payloadSize=100 --payloadCamera=1500 --DataMode="MCS2_0" --datarate=0.65 --bandWidth=2 --rho="100" --RAWConfigFile="./OptimalRawGroup/RawConfig-4-1-2.txt" --TrafficType="CameraAndSensor" --pagePeriod=4 --pageSliceLength=2 --pageSliceCount=2" > SAIDA.txt 2>&1


./waf --run "scratch/test/test --seed=1 --simulationTime=60 --payloadSize=100 --payloadCamera=1500 --DataMode="MCS2_0" --datarate=0.65 --bandWidth=2 --rho="100" --RAWConfigFile="./OptimalRawGroup/RawConfig-4-1-2.txt" --TrafficType="tcpipcamera" --pagePeriod=4 --pageSliceLength=31 --pageSliceCount=2" > SAIDA.txt 2>&1

0	1	1	47	2	0	41	63
1
0	1	1	47	2	0	64	80


UDP CAMERA:

./waf --run "scratch/test/test --seed=1 --simulationTime=60 --payloadSize=100 --payloadCamera=1500 --BeaconInterval=28672 --DataMode="MCS2_8" --datarate=7.8 --bandWidth=2 --rho="100" --RAWConfigFile="./OptimalRawGroup/RawConfig-80-10-10.txt" --TrafficType="tccamera" --pagePeriod=1 --pageSliceLength=31 --pageSliceCount=0"

TCP SENSOR:

./waf --run "scratch/test/test --seed=1 --simulationTime=60 --payloadSize=150 --payloadCamera=1500 --BeaconInterval=70400 --DataMode="MCS2_8" --datarate=7.8 --bandWidth=2 --rho="100" --RAWConfigFile="./OptimalRawGroup/RawConfig-320-10-5.txt" --TrafficType="tcpsensor" --pagePeriod=1 --pageSliceLength=31 --pageSliceCount=0"

9-> 80 -> 12640
9-> 160 -> 25280

TCP FIRMWARE

./waf --run "scratch/test/test --seed=1 --simulationTime=60 --payloadSize=1024 --payloadCamera=1500 --BeaconInterval=5500 --DataMode="MCS2_8" --datarate=7.8 --bandWidth=2 --rho="100" --RAWConfigFile="./OptimalRawGroup/RawConfig-5-1-1.txt" --TrafficType="tcpfirmware" --pagePeriod=1 --pageSliceLength=31 --pageSliceCount=0"

#Using GDB
./waf --run "scratch/test/test" --command-template="gdb %s"

run --seed=1 --runSeed=1 --simulationTime=90 --payloadSize=100 --payloadCamera=1500 --BeaconInterval=351232 --DataMode="MCS2_8" --datarate=7.8 --bandWidth=2 --rho="100" --RAWConfigFile="./OptimalRawGroup/RawConfig-2560.txt" --TrafficType="mix" --pagePeriod=1 --pageSliceLength=31 --pageSliceCount=0

run --seed=1 --runSeed=1 --simulationTime=90 --payloadSize=100 --payloadCamera=1500 --BeaconInterval=351232 --DataMode="MCS2_8" --datarate=7.8 --bandWidth=2 --rho="100" --RAWConfigFile="./OptimalRawGroup/RawConfig-2560.txt" --TrafficType="mix" --pagePeriod=1 --pageSliceLength=31 --pageSliceCount=0 > SAIDA.txt 2>&1



